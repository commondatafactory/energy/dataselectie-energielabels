#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

REV=`git rev-parse --short HEAD`

docker build -t registry.gitlab.com/commondatafactory/energy/dataselectie-energielabels:$REV .
docker push registry.gitlab.com/commondatafactory/energy/dataselectie-energielabels:$REV
